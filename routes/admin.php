<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('employeds', ['as' => 'employeds', 'uses'=>'EmployedController@index']);
Route::post('employed/store', ['as' => 'employed/store', 'uses'=>'EmployedController@store']);
Route::post('employed/update', ['as' => 'employed/update', 'uses'=>'EmployedController@update']);
Route::post('employed/updateStatus', ['as' => 'employed/updateStatus', 'uses'=>'EmployedController@updateStatus']);
Route::get('employed/delete/{id}', ['as' => 'employed/delete', 'uses'=>'EmployedController@destroy']);
Route::post('employed/searchEmployedId', ['as' => 'employed/searchEmployedId', 'uses'=>'EmployedController@searchEmployedId']);
Route::get('employed/history/{id}', ['as' => 'employed/history', 'uses'=>'EmployedController@history']);
Route::post('history/search/{id}', ['as' => 'history/search', 'uses'=>'EmployedController@searchHistory']);


Route::get('users', ['as' => 'users', 'uses'=>'UserController@index']);
Route::post('user/store', ['as' => 'user/store', 'uses'=>'UserController@store']);
Route::post('user/update', ['as' => 'user/update', 'uses'=>'UserController@update']);
Route::post('user/updateStatus', ['as' => 'user/updateStatus', 'uses'=>'UserController@updateStatus']);
Route::get('user/delete/{id}', ['as' => 'user/delete', 'uses'=>'UserController@destroy']);
Route::post('user/searchUserId', ['as' => 'user/searchUserId', 'uses'=>'UserController@searchUserId']);
Route::get('user/history/{id}', ['as' => 'user/history', 'uses'=>'UserController@history']);
Route::post('history/search/user/{id}', ['as' => 'history/search/user', 'uses'=>'UserController@searchHistory']);

Route::get('historyAll', ['as' => 'historyAll', 'uses'=>'EmployedController@historyAll']);
Route::post('history/all', ['as' => 'history/all', 'uses'=>'EmployedController@searchHistoryAll']);
