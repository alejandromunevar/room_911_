$(function() {

  //setup CSRF-TOKEN
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  //inital setup attributes
  $( "#initialAccess" ).datepicker()
  $( "#initialAccess" ).datepicker("option", "dateFormat", 'yy-mm-dd')
  $( "#finalAccess" ).datepicker()
  $( "#finalAccess" ).datepicker("option", "dateFormat", 'yy-mm-dd')


  $.extend($.fn.dataTableExt.oStdClasses, {
    "sFilterInput": "form-control",
  });

  $('#list-employed').DataTable({
    lengthChange: false,
    oLanguage: {
      sSearch: "Fullname / Employed Id / Department:"
    }
  })
  var status

  //method Send Request
  function sendRequest(data, url) {
    $.ajax({
      type: 'post',
      url: url,
      data: data,
      success: function(result){
        location.reload();
      },
      error: function (error) {
        alert('employed already exists')
      }
    })
  }
  // method Add Employed
  function addEmployed() {
    let data = {
      'internalNumber': $("#internalNumber").val(),
      'firstname': $("#firstname").val(),
      'middleName': $("#middleName").val(),
      'lastname': $("#lastname").val(),
      'department': $("#department").val()
    }
    sendRequest(data, "employed/store")
  }

  // method Edit Employed
  function editEmployed() {
    let data = {
      'id': $("#id").val(),
      'internalNumber': $("#EinternalNumber").val(),
      'firstname': $("#Efirstname").val(),
      'middleName': $("#EmiddleName").val(),
      'lastname': $("#Elastname").val(),
      'department': $("#Edepartment").val()
    }
    sendRequest(data, "employed/update")
  }

  //method Enabled/Disabled
  function enabledDisabled() {
    status = status.split('-',3)
    let data = {
      id: status[1],
      status_id: status[2]
    }
    sendRequest(data, "employed/updateStatus")
  }

  // Dialog for add Employed
  $("#dialog-create-employed").dialog({
    autoOpen: false,
    height: 470,
    width: 400,
    modal: true,
    buttons: {
      "Create": addEmployed,
      "Close": function() {
        $("#dialog-create-employed").dialog('close');
      }
    },
    close: function() {
      $("#dialog-create-employed").dialog('close');
    }
  })

  $("#create-employed").click(function(event) {
    event.preventDefault()
    $("#dialog-create-employed").dialog('open')
  })

  // Dialog for edit Employed
  $("#dialog-edit-employed").dialog({
    autoOpen: false,
    height: 470,
    width: 400,
    modal: true,
    buttons: {
      "Edit": editEmployed,
      "Close": function() {
        $("#dialog-edit-employed").dialog('close');
      }
    },
    close: function() {
      $("#dialog-edit-employed").dialog('close');
    }
  })

  $(".edit-employed").click(function(event) {
    event.preventDefault()
    let id = {'id': $(this).attr("id")}
    $.ajax({
      type: 'post',
      url: "employed/searchEmployedId",
      data: id,
      success: function(result){
        result = JSON.parse(result)
        $("#id").val(result.id)
        $("#EinternalNumber").val(result.internal_number)
        $("#Efirstname").val(result.firstname)
        $("#EmiddleName").val(result.middle_name)
        $("#Elastname").val(result.lastname)
        $("#Edepartment").val(result.department_id)
        $("#dialog-edit-employed").dialog('open')
      },
      error: function (error) {
        console.log(error)
      }
    })
  })

  // Dialog message confirmation Enabled/Disabled
  $( "#dialog-confirm-enabled-disabled-employed" ).dialog({
    autoOpen: false,
    resizable: false,
    height: "auto",
    width: 400,
    modal: true,
    buttons: {
      "Accept": enabledDisabled,
      Cancel: function() {
        $(this).dialog("close");
      }
    }
  });

  $(".eenabled-disabled").click(function(event) {
    event.preventDefault()
    $("#status-message").empty();
    status = $(this).attr("id")
    let id = status.split('-',3)

    if(id[2] == 1)
      $("#status-message").append("The employee will be <strong>Disabled</strong>. Are you sure?");
    else
      $("#status-message").append("The employee will be <strong>Enabled</strong>. Are you sure?");
    $("#dialog-confirm-enabled-disabled-employed").dialog('open')
  })

})
