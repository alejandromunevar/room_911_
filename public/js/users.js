$(function() {

  //inital setup attributes
  $.extend($.fn.dataTableExt.oStdClasses, {
    "sFilterInput": "form-control",
  });

  $('#list-user').DataTable({
    lengthChange: false,
    oLanguage: {
      sSearch: "Search:"
    }
  })
  var status

  //method Send Request
  function sendRequest(data, url) {
    $.ajax({
      type: 'post',
      url: url,
      data: data,
      success: function(result){
        location.reload();
      },
      error: function (error) {
        alert('user already exists')
      }
    })
  }
  // method Add user
  function addUser() {
    let data = {
      'username': $("#username").val(),
      'password': $("#password").val(),
    }
    sendRequest(data, "user/store")
  }

  // method Edit user
  function editUser() {
    let data = {
      'id': $("#id").val(),
      'username': $("#Eusername").val(),
      'password': $("#Epassword").val(),
    }
    sendRequest(data, "user/update")
  }

  //method Enabled/Disabled
  function enabledDisabled() {
    status = status.split('-',3)
    let data = {
      id: status[1],
      status_id: status[2]
    }
    sendRequest(data, "user/updateStatus")
  }

  // Dialog for add user
  $("#dialog-create-user").dialog({
    autoOpen: false,
    height: 270,
    width: 400,
    modal: true,
    buttons: {
      "Create": addUser,
      "Close": function() {
        $("#dialog-create-user").dialog('close');
      }
    },
    close: function() {
      $("#dialog-create-user").dialog('close');
    }
  })

  $("#create-user").click(function(event) {
    event.preventDefault()
    $("#dialog-create-user").dialog('open')
  })

  // Dialog for edit user
  $("#dialog-edit-user").dialog({
    autoOpen: false,
    height: 270,
    width: 400,
    modal: true,
    buttons: {
      "Edit": editUser,
      "Close": function() {
        $("#dialog-edit-user").dialog('close');
      }
    },
    close: function() {
      $("#dialog-edit-user").dialog('close');
    }
  })

  $(".edit-user").click(function(event) {
    event.preventDefault()
    let id = {'id': $(this).attr("id")}
    $.ajax({
      type: 'post',
      url: "user/searchUserId",
      data: id,
      success: function(result){
        result = JSON.parse(result)
        $("#id").val(result.id)
        $("#Eusername").val(result.username)
        $("#dialog-edit-user").dialog('open')
      },
      error: function (error) {
        console.log(error)
      }
    })
  })

  // Dialog message confirmation Enabled/Disabled
  $( "#dialog-confirm-enabled-disabled" ).dialog({
    autoOpen: false,
    resizable: false,
    height: "auto",
    width: 400,
    modal: true,
    buttons: {
      "Accept": enabledDisabled,
      Cancel: function() {
        $(this).dialog("close");
      }
    }
  });

  $(".enabled-disabled").click(function(event) {
    event.preventDefault()
    $("#status-message").empty();
    status = $(this).attr("id")
    if(status == 'status1')
      $("#status-message").append("The employee will be <strong>Disabled</strong>. Are you sure?");
    else
      $("#status-message").append("The employee will be <strong>Enabled</strong>. Are you sure?");
    $("#dialog-confirm-enabled-disabled").dialog('open')
  })

})
