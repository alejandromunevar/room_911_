<section class="col-md-12">
    <div class="d-flex align-items-end flex-column">
        <div class="mb-auto p-2">
            <a id="create-user" class="btn btn-primary btn-xs w-100" href="">New User Admin</a>
        </div>
    </div>
    <h5>Search by:</h5>
</section>

<section class="mt-3 table-responsive">
    <table id="list-user" class="table table-striped display">
        <thead>
            <tr class="text-center">
                <th>Username</th>
                <th>Role</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr class="text-center">
                    <td>{{$user->username}}</td>
                    <td>{{$user->role}}</td>
                    <td>{{$user->status->name}}</td>
                    <td>
                        <div class="row col-md-12">
                            @if(\Auth::user()->id != $user->id)
                                <div class="mr-1">
                                    <a id="{{$user->id}}" class="edit-user btn btn-primary btn-xs w-100" href="">Update</a>
                                </div>
                                <div class="mr-1">
                                    @if($user->status_id == 1)
                                        <a id="status-{{$user->id}}-{{$user->status_id}}" class="btn btn-danger btn-xs w-100 enabled-disabled" href="">Disabled</a>
                                    @else
                                        <a id="status-{{$user->id}}-{{$user->status_id}}" class="btn btn-primary btn-xs w-100 enabled-disabled" href="">Enabled</a>
                                    @endif
                                </div>
                                <div class="mr-1">
                                    <a class="btn btn-danger btn-xs w-100"  href="{{url('user/delete',$user->id)}}">Delete</a>
                                </div>
                            @endif
                            <div>
                                <a class="btn btn-primary btn-xs w-100" href="{{url('user/history',$user->id)}}">History</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach()
        </tbody>
    </table>
</section>

@include('admin.users.create')
@include('admin.users.edit')

<div id="dialog-confirm-enabled-disabled" title="Enabled/Disabled User ?">
    <p>
        <span id="status-message"></span>
    </p>
</div>
