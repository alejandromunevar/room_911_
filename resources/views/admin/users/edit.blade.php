<div id="dialog-edit-user" title="Edit user">
    <form>
        <fieldset>
            <div>
                <label>
                    Username
                </label>
                <input type="text" id="Eusername" name="Eusername" placeholder="Ej. admin" class="form-control" >
            </div>
            <div>
                <label>
                    Password
                </label>
                <input type="password" id="Epassword" name="Epassword" placeholder="********" class="form-control">
            </div>
            <input type="hidden" id="id">
        </fieldset>
    </form>
</div>
