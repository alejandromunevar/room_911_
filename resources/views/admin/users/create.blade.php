<div id="dialog-create-user" title="Create new user">
    <form id="form-create">
        <fieldset>
            <div>
                <label>
                    Username
                </label>
                <input type="text" id="username" name="username" placeholder="Ej. admin" class="form-control" >
            </div>
            <div>
                <label>
                    Password
                </label>
                <input type="password" id="password" name="password" placeholder="*******" class="form-control">
            </div>
        </fieldset>
    </form>
</div>
