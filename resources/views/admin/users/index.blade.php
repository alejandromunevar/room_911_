@extends('layouts.app')

@section('content')
<div class="m-3">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">
                    <h2>Users Admin Module</h2>
                </div>

                <div class="card-body">
                    @include('admin.users.list')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
