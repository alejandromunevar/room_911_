<section class="col-md-12">
    <div class="d-flex align-items-end flex-column">
        <div class="mb-auto p-2">
            <a id="create-employed" class="btn btn-primary btn-xs w-100" href="">New Employed</a>
        </div>
    </div>
    <h5>Search by:</h5>
</section>

<section class="mt-3 table-responsive">
    <table id="list-employed" class="table table-striped display">
        <thead>
            <tr class="text-center">
                <th>Employed Id</th>
                <th>Department</th>
                <th>Lastname</th>
                <th>Middle name</th>
                <th>Firstname</th>
                <th>Last access</th>
                <th>Total access</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($employeds as $employed)
                <tr class="text-center">
                    <td>{{$employed['employed']->internal_number}}</td>
                    <td>{{$employed['employed']->department->name}}</td>
                    <td>{{$employed['employed']->lastname}}</td>
                    <td>{{$employed['employed']->middle_name}}</td>
                    <td>{{$employed['employed']->firstname}}</td>
                    <td>{{$employed['lastAccess']}}</td>
                    <td>{{$employed['countTracing']}}</td>
                    <td>
                        <div class="row col-md-12">
                            <div class="mr-1">
                                <a id="{{$employed['employed']->id}}" class="edit-employed btn btn-primary btn-xs w-100" href="">Update</a>
                            </div>
                            <div class="mr-1">
                                @if($employed['employed']->status_id == 1)
                                    <a id="Estatus-{{$employed['employed']->id}}-{{$employed['employed']->status_id}}" class="btn btn-danger btn-xs w-100 eenabled-disabled" href="">Disabled</a>
                                @else
                                    <a id="Estatus-{{$employed['employed']->id}}-{{$employed['employed']->status_id}}" class="btn btn-primary btn-xs w-100 eenabled-disabled" href="">Enabled</a>
                                @endif
                            </div>
                            <div class="mr-1">
                                <a class="btn btn-danger btn-xs w-100"  href="{{url('employed/delete',$employed['employed']->id)}}">Delete</a>
                            </div>
                            <div>
                                <a class="btn btn-primary btn-xs w-100" href="{{url('employed/history',$employed['employed']->id)}}">History</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach()
        </tbody>
    </table>
</section>

@include('admin.employeds.create')
@include('admin.employeds.edit')

<div id="dialog-confirm-enabled-disabled-employed" title="Enabled/Disabled employed ?">
    <p>
        <span id="status-message"></span>
    </p>
</div>
