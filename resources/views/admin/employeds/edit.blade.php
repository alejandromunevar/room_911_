<div id="dialog-edit-employed" title="Edit employed">
    <form>
        <fieldset>
            <div>
                <label>
                    Internal Number
                </label>
                <input type="text" id="EinternalNumber" placeholder="Ej. 12345678" class="form-control">
            </div>
            <div>
                <label>
                    Firstname
                </label>
                <input type="text" id="Efirstname" placeholder="Ej. Juan" class="form-control">
            </div>
            <div>
                <label>
                    Middle Name
                </label>
                <input type="text" id="EmiddleName" placeholder="Ej. Manuel" class="form-control">
            </div>
            <div>
                <label>
                    Lastname
                </label>
                <input type="text" id="Elastname" placeholder="Ej. Perez" class="form-control">
            </div>
            <div>
                <label>
                    Department
                </label>
                <select id="Edepartment" class="form-control">
                    <option value="">Select...</option>
                    @foreach($departments as $department)
                        <option value="{{$department->id}}">{{$department->name}}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" id="id">
        </fieldset>
    </form>
</div>
