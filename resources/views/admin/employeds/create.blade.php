<div id="dialog-create-employed" title="Create new employed">
    <form id="form-create">
        <fieldset>
            <div>
                <label>
                    Internal Number
                </label>
                <input type="text" id="internalNumber" name="internalNumber" placeholder="Ej. 12345678" class="form-control" >
            </div>
            <div>
                <label>
                    Firstname
                </label>
                <input type="text" id="firstname" name="firstname" placeholder="Ej. Juan" class="form-control" >
            </div>
            <div>
                <label>
                    Middle Name
                </label>
                <input type="text" id="middleName" name="middleName" placeholder="Ej. Manuel" class="form-control">
            </div>
            <div>
                <label>
                    Lastname
                </label>
                <input type="text" id="lastname" name="lastname" placeholder="Ej. Perez" class="form-control" >
            </div>
            <div>
                <label>
                    Department
                </label>
                <select id="department" name="department" class="form-control" >
                    <option value="">Select...</option>
                    @foreach($departments as $department)
                        <option value="{{$department->id}}">{{$department->name}}</option>
                    @endforeach
                </select>
            </div>
        </fieldset>
    </form>
</div>
