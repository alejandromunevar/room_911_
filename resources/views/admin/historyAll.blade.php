@extends('layouts.app')

@section('content')
<div class="m-3">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">
                    <h2>Access History</h2>
                </div>

                <div class="card-body">
                    <section class="col-md-12">
                        <h5>Search by:</h5>
                        <form action="{{ url('history/all') }}" method="post">
                            @csrf
                            <div class="row col-md-12">
                                <div class="col-md-5">
                                    <label>Initial access date</label>
                                    <input type="text" placeholder="dd/mm/aaaa" name="initialAccess" id="initialAccess" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <label>Final access date</label>
                                    <input type="text" placeholder="dd/mm/aaaa" name="finalAccess" id="finalAccess" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <button class=" mt-4 btn btn-primary">Search</button>
                                </div>
                            </div>
                        </form>
                    </section>

                    <section class="mt-3 table-responsive">
                        <table id="list-history" class="table table-striped display">
                            <thead>
                                <tr class="text-center">
                                    <th>Employed</th>
                                    <th>Access date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tracingEmployeds as $tracingEmployed)
                                    <tr class="text-center">
                                        <td>{{$tracingEmployed->employed}}</td>
                                        <td>{{$tracingEmployed->created_at}}</td>
                                        <td>{{$tracingEmployed->status->name}}</td>
                                    </tr>
                                @endforeach()
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
