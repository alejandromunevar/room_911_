@extends('layouts.app')

@section('content')
<div class="m-3">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">
                    <h2>Access Control ROOM_911</h2>
                </div>

                <div class="card-body">
                    <div class="col-md-12 d-flex justify-content-center">
                        <div class="col-md-8 row">
                            <div class="col-md-6">
                                <a class="btn btn-primary w-100" href="{{url('users')}}">Users Admin Module</a>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-primary w-100" href="{{url('employeds')}}">Employeds Module</a>
                            </div>
                            <div class="col-md-6 mt-3">
                                <a class="btn btn-primary w-100" href="{{url('historyAll')}}">Access History</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
