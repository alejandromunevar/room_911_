<?php

namespace Room_911;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    protected $fillable = ['name'];

    public function employeds()
    {
        return $this->hasMany('Room_911\Employed');
    }
}
