<?php

namespace Room_911;

use Illuminate\Database\Eloquent\Model;

class TracingAccess extends Model
{
    protected $table = 'tracing_accesses';
    protected $fillable = ['employed','status_id'];

    public function status()
    {
        return $this->belongsTo('Room_911\Status');
    }
}
