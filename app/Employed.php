<?php

namespace Room_911;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employed extends Model
{
    protected $table = 'employeds';
    protected $fillable = ['internal_number','firstname','middle_name','lastname','department_id','status_id'];

    use SoftDeletes;

    public function department()
    {
        return $this->belongsTo('Room_911\Department');
    }

    public function status()
    {
        return $this->belongsTo('Room_911\Status');
    }
}
