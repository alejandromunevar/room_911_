<?php

namespace Room_911\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;

use Closure;

class AdminMiddleware
{
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->role == 'admin_room_911' && \Auth::user()->status_id == 1)
            return $next($request);

        $this->auth->logout();
        if ($request->ajax())
            return response('Unauthorized.', 401);
        else {
            flash('You do not have permission for the requested resource.')->error();
            return redirect()->to('/login');
        }

    }
}
