<?php

namespace Room_911\Http\Controllers;

use Illuminate\Http\Request;
use Room_911\User;
use Room_911\TracingAccess;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tracingUser = TracingAccess::where('employed',\Auth::user()->username)->get();
        $users = User::all();
        return view('admin.users.index',['tracingUser' => $tracingUser->last(), 'users' => $users]);
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->role = 'admin_room_911';
        $user->status_id = 1;
        $user->save();
        return json_encode($user);
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->username = $request->username;
        $user->password = !is_null($request->password) ? Hash::make($request->password): $user->password;
        $user->save();
        return json_encode($user);
    }

    public function updateStatus(Request $request)
    {
        $user = User::find($request->id);
        $user->status_id = $request->status_id == 1 ? 2: 1;
        $user->save();
        return json_encode($user);
    }

    public function searchUserId(Request $request)
    {
        $user = User::find($request->id);
        return json_encode($user);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back();
    }

    public function history($id)
    {
        $user = User::find($id);
        $tracingEmployeds = TracingAccess::where('employed',$user->username)->get();
        return view('admin.users.history',['tracingEmployeds' => $tracingEmployeds, 'user' => $user]);
    }

    public function searchHistory($id, Request $request)
    {
        $user = User::find($id);
        $initialAccess = $request->initialAccess." 00:00:00";
        $finalAccess= $request->finalAccess." 00:00:00";
        $tracingEmployeds = TracingAccess::where('employed',$user->username)
                                        ->whereBetween('created_at',[$initialAccess, $finalAccess])
                                        ->get();
        return view('admin.users.history',['tracingEmployeds' => $tracingEmployeds, 'user' => $user]);
    }
}
