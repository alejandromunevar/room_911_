<?php

namespace Room_911\Http\Controllers;
use Room_911\TracingAccess;
use Room_911\Employed;
use Room_911\Department;
use Carbon\Carbon;


use Illuminate\Http\Request;

class EmployedController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tracingUser = TracingAccess::where('employed',\Auth::user()->username)->get();
        $employeds = Employed::all();
        $employeds = $this->employedTracings($employeds);
        $departments = Department::all();
        return view('admin.employeds.index',['tracingUser' => $tracingUser->last(), 'employeds' => $employeds, 'departments' => $departments]);
    }

    public function employedTracings($employeds)
    {
        $arraytracing = array();
        foreach ($employeds as $employed) {
            $employed->department;
            $countTracing = TracingAccess::where('employed',$employed->internal_number)->get();
            if(count($countTracing) > 0)
                array_push($arraytracing, [
                    'employed' => $employed,
                    'countTracing' => $countTracing->count(),
                    'lastAccess' => $countTracing->last()->created_at
                ]);
            else
                array_push($arraytracing, [
                    'employed' => $employed,
                    'countTracing' => 0,
                    'lastAccess' => '-'
                ]);
        }
        return $arraytracing;
    }

    public function store(Request $request)
    {
        $employed = new Employed;
        $employed->internal_number = $request->internalNumber;
        $employed->firstname = $request->firstname;
        $employed->middle_name = $request->middleName;
        $employed->lastname = $request->lastname;
        $employed->department_id = $request->department;
        $employed->status_id = 1;
        $employed->save();
        return json_encode($employed);
    }

    public function update(Request $request)
    {
        $employed = Employed::find($request->id);
        $employed->internal_number = $request->internalNumber;
        $employed->firstname = $request->firstname;
        $employed->middle_name = $request->middleName;
        $employed->lastname = $request->lastname;
        $employed->department_id = $request->department;
        $employed->status_id = 1;
        $employed->save();
        return json_encode($employed);
    }

    public function updateStatus(Request $request)
    {
        $employed = Employed::find($request->id);
        $employed->status_id = $request->status_id == 1 ? 2: 1;
        $employed->save();
        return json_encode($employed);
    }

    public function searchEmployedId(Request $request)
    {
        $employed = Employed::find($request->id);
        return json_encode($employed);
    }

    public function destroy($id)
    {
        $employed = Employed::find($id);
        $employed->delete();
        return redirect()->back();
    }

    public function history($id)
    {
        $employed = Employed::find($id);
        $tracingEmployeds = TracingAccess::where('employed',$employed->internal_number)->get();
        return view('admin.employeds.history',['tracingEmployeds' => $tracingEmployeds, 'employed' => $employed]);
    }

    public function accessControl(Request $request)
    {
        $employed = Employed::where('internal_number',$request->internalNumber)->first();
        if(!is_null($employed)){
            if($employed->status_id === 1) {
                $tracingAccess = new TracingAccess;
                $tracingAccess->employed = $request->internalNumber;
                $tracingAccess->status_id = 1;
                $tracingAccess->save();
                flash('Success access!')->error();
                return redirect()->back();
            }
            if($employed->status_id === 2) {
                $tracingAccess = new TracingAccess;
                $tracingAccess->employed = $request->internalNumber;
                $tracingAccess->status_id = 4;
                $tracingAccess->save();
                flash('You do not have permissions!')->error();
                return redirect()->back();
            }
        }
        else {
            $tracingAccess = new TracingAccess;
            $tracingAccess->employed = $request->internalNumber;
            $tracingAccess->status_id = 5;
            $tracingAccess->save();
            flash('User not exists')->error();
            return redirect()->back();
        }
    }

    public function searchHistory($id, Request $request)
    {
        $employed = Employed::find($id);
        $initialAccess = $request->initialAccess." 00:00:00";
        $finalAccess= $request->finalAccess." 00:00:00";
        $tracingEmployeds = TracingAccess::where('employed',$employed->internal_number)
                                        ->whereBetween('created_at',[$initialAccess, $finalAccess])
                                        ->get();
        return view('admin.employeds.history',['tracingEmployeds' => $tracingEmployeds, 'employed' => $employed]);
    }

    public function historyAll()
    {
        $tracingEmployeds = TracingAccess::all();
        return view('admin.historyAll',['tracingEmployeds' => $tracingEmployeds]);
    }

    public function searchHistoryAll(Request $request)
    {
        $initialAccess = $request->initialAccess." 00:00:00";
        $finalAccess= $request->finalAccess." 00:00:00";
        $tracingEmployeds = TracingAccess::whereBetween('created_at',[$initialAccess, $finalAccess])->get();
        return view('admin.historyAll',['tracingEmployeds' => $tracingEmployeds]);
    }
}
