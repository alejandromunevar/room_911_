<?php

namespace Room_911\Http\Controllers\Auth;

use Room_911\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Room_911\TracingAccess;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if(\Auth::user()->status_id != 1) {
            \Auth::logout();
            flash('You do not have permissions.')->error();
            $tracingAccess = new TracingAccess;
            $tracingAccess->employed = \Auth::user()->username;
            $tracingAccess->status_id = 4;
            $tracingAccess->save();
            return redirect()->to('/login');
        }

        $tracingAccess = new TracingAccess;
        $tracingAccess->employed = \Auth::user()->username;
        $tracingAccess->status_id = 3;
        $tracingAccess->save();

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }
}
