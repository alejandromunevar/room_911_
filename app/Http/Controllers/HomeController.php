<?php

namespace Room_911\Http\Controllers;

use Illuminate\Http\Request;
use Room_911\TracingAccess;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tracingUser = TracingAccess::where('employed',\Auth::user()->username)->get();
        return view('home',['tracingUser' => $tracingUser->last()]);
    }
}
