<?php

namespace Room_911;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';
    protected $fillable = ['name'];

    public function employeds()
    {
        return $this->hasMany('Room_911\Employed');
    }

    public function tracingAccesses()
    {
        return $this->hasMany('Room_911\TracingAccess');
    }
}
