<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table)
        {
            $table->foreign('status_id')->references('id')->on('statuses');
        });

        Schema::table('employeds', function ($table)
        {
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('status_id')->references('id')->on('statuses');
        });

        Schema::table('tracing_accesses', function ($table)
        {
            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
