<?php

use Illuminate\Database\Seeder;
use Room_911\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new Status;
        $status->name = "Enabled";
        $status->save();

        $status = new Status;
        $status->name = "Disabled";
        $status->save();

        $status = new Status;
        $status->name = "Connected";
        $status->save();

        $status = new Status;
        $status->name = "Permissions Error";
        $status->save();

        $status = new Status;
        $status->name = "User not exist";
        $status->save();
    }
}
