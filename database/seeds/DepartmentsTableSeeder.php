<?php

use Illuminate\Database\Seeder;
use Room_911\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = new Department;
        $departments->name = "Department 1";
        $departments->save();

        $departments = new Department;
        $departments->name = "Department 2";
        $departments->save();
    }
}
