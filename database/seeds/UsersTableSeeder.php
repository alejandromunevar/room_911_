<?php

use Illuminate\Database\Seeder;
use Room_911\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->username = 'admin';
        $user->password = Hash::make('admin123');
        $user->role = 'admin_room_911';
        $user->status_id = 1;
        $user->save();
    }
}
